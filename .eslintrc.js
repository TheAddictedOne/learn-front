module.exports = {
  extends: 'standard',
  installedESLint: true,
  globals: {
    caches: false,
    fetch: false,
    self: false
  },
  plugins: [
    'standard'
  ],
  parserOptions: {
    ecmaVersion: 6
  },
  rules: {
    'func-call-spacing': 0,
    'no-global-assign': 0,
    'no-multiple-empty-lines': 0,
    'no-tabs': 0,
    'no-template-curly-in-string': 0,
    'no-unsafe-negation': 0
  }
};
