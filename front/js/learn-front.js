const ENABLE_SW = false

// Service Worker to handle the first rendering ----------------------------------------------------
if (ENABLE_SW && 'serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker
      .register('/app-shell.js')
      .then(() => console.log('Service Worker Registered'))
      .catch(() => console.log('Failed to register Service Worker'))
  })
}

// Get GoT episodes
const got = document.querySelector('#js-got')

const processGot = (response) => {
  response.json().then((json) => {
    got.innerHTML = json.map((li) => `<li>${li}</li>`).join('')
  })
}

fetch('/api/got')
  .then(processGot)
  .catch((data) => console.log(data))

// Get tWD episodes
const twd = document.querySelector('#js-twd')

const processTwd = (response) => {
  response.json().then((json) => {
    twd.innerHTML = json.map((li) => `<li>${li}</li>`).join('')
  })
}

fetch('/api/twd')
  .then(processTwd)
  .catch((data) => console.log(data))
