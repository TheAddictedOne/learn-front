var BodyParser = require('body-parser')
var CookieParser = require('cookie-parser')
var express = require('express')
var nunjucks = require('nunjucks')

var ApiRoutes = require('./back/routes/Api')

// Initialization ----------------------------------------------------------------------------------
const PORT = 4242

var app = express()

nunjucks.configure('back/views', {
  autoescape: true,
  watch: true,
  express: app
})

// Configuration -----------------------------------------------------------------------------------
app.set('view engine', 'njk')
app.set('view cache', 'false')

app.use(express.static(`${__dirname}/public`))
app.use(CookieParser())
app.use(BodyParser.json({ limit: '10mb' }))
app.use(BodyParser.urlencoded({
  extended: true,
  limit: '10mb'
}))

app.get('/', (req, res) => res.render('index'))
app.use('/api', ApiRoutes)


// Start ! -----------------------------------------------------------------------------------------
app.listen(PORT, () => console.log(`[Server] Listening on ${PORT}`))
