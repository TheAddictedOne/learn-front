var router = require('express').Router()

router.route('/got')
  .get((req, res) => {
    return res.status(200).json([
      'Winter is coming',
      'The Kingsroad',
      'Lord Snow',
      'Cripples, Bastards, and Broken Things',
      'The Wolf and the Lion',
      'A Golden Crown',
      'You Win or You Die',
      'The Pointy End',
      'Baelor',
      'Fire and Blood'
    ])
  })

router.route('/twd')
  .get((req, res) => {
    return res.status(200).json([
      'Days Gone Bye',
      'Guts',
      'Tell it to the Frogs',
      'Vatos',
      'Wildfire',
      'TS-19'
    ])
  })

module.exports = router
