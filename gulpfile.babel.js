import buffer from 'vinyl-buffer'
import browserify from 'browserify'
import bs from 'browser-sync'
import del from 'del'
import gulp from 'gulp'
import merge from 'merge-stream'
import plugins from 'gulp-load-plugins'
import source from 'vinyl-source-stream'
import watchify from 'watchify'

// Initialization ----------------------------------------------------------------------------------
const SASS = {
  ALL: 'front/sass/**/*.sass',
  DEST: 'public/css',
  OUTPUT: 'learn-front.css',
  SRC: 'front/sass/main.sass'
}

const BACKEND = ['back/**/*', 'server.js']

const $ = plugins()

// browser-sync ------------------------------------------------------------------------------------
/**
 * Instanciate a BrowserSync instance, with some default options
 * @see https://www.browsersync.io/
 */
let BrowserSync = bs.create()
BrowserSync.init({
  open: false,
  logPrefix: 'BrowserSync',
  proxy: {
    target: 'http://localhost:4242',
    ws: true
  },
  notify: false
})

// Tasks -------------------------------------------------------------------------------------------

gulp.task('del', () => del(['public/css/*', 'public/js/*']))

// Styles
gulp.task('styles', () => gulp.src(SASS.SRC)
  .pipe($.plumber({ handleError: $.util.log }))
  .pipe($.sourcemaps.init())
  .pipe($.sass().on('error', $.sass.logError))
  .pipe($.sourcemaps.write())
  .pipe($.rename(SASS.OUTPUT))
  .pipe(gulp.dest(SASS.DEST))
  .pipe(BrowserSync.reload({stream: true}))
)

// Main
/**
 * Entry points
 */
const entries = ['learn-front.js', 'app-shell.js']

/**
 * Output a file using browserify
 * @param  {BrowserifyInstance} instance
 * @return {Stream} stream
 */
const bundle = function (instance) {
  return instance
    .bundle()
    .on('error', $.util.log)
    .pipe(source(instance._options.entries))
    .pipe(buffer())
    .pipe($.sourcemaps.init({loadMaps: true}))
    // .pipe($.uglify())
    .pipe($.sourcemaps.write('./'))
    .pipe(gulp.dest('./public/'))
    .pipe(BrowserSync.reload({stream: true}))
}

const streams = entries.map((entry) => {
  const options = {
    cache: {},
    packageCache: {},
    entries: entry,
    basedir: 'front/js/',
    debug: true,
    transform: ['babelify']
  }
  let instance = watchify(browserify(options))

  instance.on('update', function () {
    bundle(instance)
  })
  instance.on('log', $.util.log)

  return bundle(instance)
})

const scripts = function () {
  return merge.apply(merge, streams)
}

gulp.task('scripts', scripts)

gulp.task('watch', () => {
  gulp.watch(SASS.ALL, ['styles'])
  gulp.watch(BACKEND, () => {
    setTimeout(BrowserSync.reload, 500)
  })
})

gulp.task('build', ['styles', 'scripts'])
gulp.task('prod', ['prod-styles', 'prod-scripts'])
gulp.task('default', ['build', 'watch'])
